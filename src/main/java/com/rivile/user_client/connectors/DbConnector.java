package com.rivile.user_client.connectors;

import com.rivile.user_client.dto.QueryResult;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class DbConnector {

    QueryResult executeQuery(Connection connection, String query) {
        QueryResult queryResult = new QueryResult();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int colCount = rs.getMetaData().getColumnCount();

            List<String> headers = new ArrayList<>();
            List<String> data = new ArrayList<>();

            // SQL uses 1-indexed rows
            for (int col = 1; col <= colCount; col++) {
                headers.add(rs.getMetaData().getColumnName(col));
            }
            while (rs.next()) {
                String dataColSeparator = ";";
                List dataRow = new ArrayList<String>();
                for (int col = 1; col <= colCount; col++) {
                    dataRow.add(rs.getString(col));
                }
                data.add(String.join(dataColSeparator, dataRow));
            }

            System.out.println("Columns:" + headers);
            System.out.println("Results" + data.toString());
            queryResult.setHeaders(headers);
            queryResult.setData(data);
            return queryResult;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            queryResult.setError(e.getMessage());
            return queryResult;
        }
    }

    public abstract QueryResult executeQuery(String query);

    QueryResult updateProcedure(Connection connection, String name, String version, String updatedProcedure) {
        QueryResult queryResult = new QueryResult();
        int result;
        try {
            Statement stmt = connection.createStatement();
            result = stmt.executeUpdate(updatedProcedure);
            if (result != 0) {
                System.out.println("Procedure update not successful. Procedure not saved.");
                queryResult.setError("Procedure update not successful. Procedure not saved.");
            }
            queryResult.setData(Arrays.asList("procedure updated"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            queryResult.setError(e.getMessage());
            e.printStackTrace();
        }
        return queryResult;
    }

    public abstract QueryResult updateProcedure(String name, String version, String updatedProcedure);

    QueryResult executeUpdate(Connection connection, String query) {
        QueryResult queryResult = new QueryResult();
        int rowsUpdated;
        try {
            Statement stmt = connection.createStatement();
            rowsUpdated = stmt.executeUpdate(query);
            if (rowsUpdated == 0) {
                System.out.println("No rows were updated.");
                queryResult.setError("No rows were updated.");
            }
            queryResult.setData(Arrays.asList("procedure updated " + rowsUpdated + " rows."));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            queryResult.setError(e.getMessage());
            e.printStackTrace();
        }
        return queryResult;
    }

    public abstract QueryResult executeUpdate(String query);
}
