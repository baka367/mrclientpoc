package com.rivile.user_client.connectors;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.rivile.user_client.config.DbConfig;
import com.rivile.user_client.dto.QueryResult;

import java.sql.Connection;
import java.sql.SQLException;

public class SqlserverConnector extends DbConnector{

    private Connection con;

    public SqlserverConnector(DbConfig dbConfig) {
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setServerName(dbConfig.getHost());
            ds.setPortNumber(Integer.parseInt(dbConfig.getPort()));
            ds.setDatabaseName(dbConfig.getDb());
            ds.setUser(dbConfig.getUsername());
            ds.setPassword(dbConfig.getPassword());

            con = ds.getConnection();

        } catch(Exception e){
            System.out.println(e.getMessage());
            System.exit(1);
        } finally {
            System.out.println("Successfully connected to mysql server@" + dbConfig.getHost());
        }
    }

    @Override
    public QueryResult executeQuery(String query) {
        return this.executeQuery(con, query);
    }

    @Override
    public QueryResult executeUpdate(String query) {
        return this.executeUpdate(con, query);
    }

    @Override
    public QueryResult updateProcedure(String name, String version, String updatedProcedure) {
        return this.updateProcedure(con, name, version, updatedProcedure);
    }

    public void closeConnection() {
        try {
            this.con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
