package com.rivile.user_client.connectors;

import com.rivile.user_client.config.DbConfig;
import com.rivile.user_client.dto.QueryResult;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class MysqlConnector extends DbConnector{

    private Connection con;

    public MysqlConnector(DbConfig dbConfig) {
        try {
            con = DriverManager.getConnection(
                    "jdbc:mysql://" + dbConfig.getHost() + ":" + dbConfig.getPort() + "/" + dbConfig.getDb(),
                    dbConfig.getUsername(), dbConfig.getPassword());

        } catch(Exception e){
            System.out.println(e.getMessage());
            System.exit(1);
        } finally {
            System.out.println("Successfully connected to mysql server@" + dbConfig.getHost());
        }
    }

    @Override
    public QueryResult executeQuery(String query) {
        return this.executeQuery(con, query);
    }

    @Override
    public QueryResult updateProcedure(String name, String version, String updatedProcedure) {
        QueryResult queryResult = new QueryResult();
        int result;
        try {
            Statement stmt = con.createStatement();
            result = stmt.executeUpdate(updatedProcedure);
            if (result != 0) {
                System.out.println("Procedure create not successful. Procedure not created.");
                queryResult.setError("Procedure create not successful. Procedure not created.");
            }
            queryResult.setData(Arrays.asList("procedure updated"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            queryResult.setError(e.getMessage());
            e.printStackTrace();
        }
        return queryResult;
    }

    @Override
    public QueryResult executeUpdate(String query) {
        return this.executeQuery(con, query);
    }
}
