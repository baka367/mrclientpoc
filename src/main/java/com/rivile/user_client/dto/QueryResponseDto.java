package com.rivile.user_client.dto;
import lombok.*;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class QueryResponseDto {
    private String id;
    private String query;
    private String result;
    private String status;
}
