package com.rivile.user_client.dto;

import lombok.*;

import java.util.List;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
public class QueryResult {
    private List<String> headers;
    private List<String> data;
    private String error;
}
