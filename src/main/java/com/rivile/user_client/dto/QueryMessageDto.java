package com.rivile.user_client.dto;
import lombok.*;
import org.codehaus.jackson.annotate.JsonRawValue;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class QueryMessageDto {
    private String requestId;
    private String procedureName;
    private String command;
    @JsonRawValue
    private String data;
}
