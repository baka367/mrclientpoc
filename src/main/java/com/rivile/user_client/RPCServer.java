package com.rivile.user_client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.rivile.user_client.config.QueryCommand;
import com.rivile.user_client.config.RabbitMqConfig;
import com.rivile.user_client.connectors.DbConnector;
import com.rivile.user_client.dto.QueryMessageDto;
import com.rivile.user_client.dto.QueryResponseDto;
import com.rivile.user_client.dto.QueryResult;

import java.io.IOException;

public class RPCServer {
    static void start(RabbitMqConfig rabbitMqConfig, Channel channel, DbConnector dbConnector) {
        try {
            channel.exchangeDeclare(rabbitMqConfig.getRpcExchange(), "direct");
            channel.queueDeclare(rabbitMqConfig.getRpcQueue(), false, false, false, null);
            channel.queueBind(rabbitMqConfig.getRpcQueue(), rabbitMqConfig.getRpcExchange(), rabbitMqConfig.getRpcRoutingKey());
            channel.queuePurge(rabbitMqConfig.getRpcQueue());

            channel.basicQos(1);

            System.out.println(" [x] Awaiting RPC requests");

//            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();

                QueryResponseDto queryResponse = new QueryResponseDto();

                try {
                    QueryMessageDto queryRequest = MessageParser.parseQueryMessage(new String(delivery.getBody()));
                    System.out.println("Received request: " + new String(delivery.getBody()));

                    if (queryRequest != null && queryRequest.getCommand() != null) {
                        QueryResult queryResult;
                        switch (queryRequest.getCommand()) {
                            case QueryCommand.CMD_UQ_SPEQ:
                                queryResult = dbConnector.executeQuery(queryRequest.getData());
                                break;
                            case QueryCommand.CMD_UQ_SPEU:
                                queryResult = dbConnector.executeUpdate(queryRequest.getData());
                                break;
                            // system commands
                            // queryResult = dbConnector.updateProcedure("test5", "123", queryRequest.getPayload());
                            default:
                                throw new RuntimeException("Unrecognised command: " + queryRequest.getCommand());

                        }
                        queryResponse.setId(queryRequest.getRequestId());
                        queryResponse.setQuery(queryRequest.getData());
                        queryResponse.setResult(MessageParser.parseQueryResult(queryResult));
                        queryResponse.setStatus(queryResult.getError() == null ? "finished" : "error");
                    }
                } catch (RuntimeException e) {
                    System.out.println(" [.] " + e.toString());
                } finally {
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, MessageParser.parseQueryResponse(queryResponse).getBytes());
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
//                    synchronized (monitor) {
//                        monitor.notify();
//                    }
                }
            };

            channel.basicConsume(rabbitMqConfig.getRpcQueue(), false, deliverCallback, (consumerTag -> { }));
            // Wait and be prepared to consume the message from RPC client.
//            while (true) {
//                synchronized (monitor) {
//                    try {
//                        monitor.wait();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        System.out.println("RPC Server started on queue \"" + rabbitMqConfig.getRpcQueue() + "\"");
    }
}
