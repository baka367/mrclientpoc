package com.rivile.user_client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rivile.user_client.config.QueryCommand;
import com.rivile.user_client.config.RabbitMqConfig;
import com.rivile.user_client.connectors.DbConnector;
import com.rivile.user_client.dto.QueryMessageDto;
import com.rivile.user_client.dto.QueryResponseDto;
import com.rivile.user_client.dto.QueryResult;

import java.io.IOException;

class AdHocConsumer {
    static boolean connectToQueue(RabbitMqConfig rabbitMqConfig, Channel channel, DbConnector dbConnector, String queue, String exchange, boolean autoAck) {
        try {
            channel.basicConsume(queue, autoAck, "myConsumerTag",
                new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag,
                                               Envelope envelope,
                                               AMQP.BasicProperties properties,
                                               byte[] body)
                            throws IOException
                    {
                        long deliveryTag = envelope.getDeliveryTag();

                        QueryMessageDto queryRequest = MessageParser.parseQueryMessage(new String(body));
                        System.out.println("Received request: " + new String(body));

                        if (queryRequest != null && queryRequest.getCommand() != null && queryRequest.getCommand().equals(QueryCommand.CMD_UQ_SPEQ)) {
                            QueryResult queryResult = dbConnector.executeQuery(queryRequest.getData());

                            QueryResponseDto queryResponse = new QueryResponseDto();
                            queryResponse.setId(queryRequest.getRequestId());
                            queryResponse.setQuery(queryRequest.getData());
                            queryResponse.setResult(MessageParser.parseQueryResult(queryResult));
                            queryResponse.setStatus(queryResult.getError() == null ? "finished" : "error");


                            sendResponse(this.getChannel(), MessageParser.parseQueryResponse(queryResponse), rabbitMqConfig.getAdHocSendBackExchange(), rabbitMqConfig.getAdHocSendBackQueue());
                            System.out.println("Sending query result: " + MessageParser.parseQueryResult(queryResult));
                            this.getChannel().basicAck(deliveryTag, false);
                        }
                    }

                });
            channel.queueDeclarePassive(queue);
            channel.queueBind(queue, exchange, "");
            System.out.println("AdHoc consumer connected on queue \"" + queue + "\"");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    static void sendResponse(Channel channel, String message, String responseExchange, String responseQueue) {
        try {
            channel.basicPublish(responseExchange, responseQueue, null, message.getBytes());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
