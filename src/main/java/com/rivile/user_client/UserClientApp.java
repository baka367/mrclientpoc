package com.rivile.user_client;

import com.rabbitmq.client.*;
import com.rivile.user_client.config.DbConfig;
import com.rivile.user_client.config.RabbitMqConfig;
import com.rivile.user_client.connectors.DbConnector;
import com.rivile.user_client.factories.DbConnectionFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

public class UserClientApp {
    public static void main(String[] args) {
        // globals
        // connection control loop params
        boolean connected = false;
        AMQP.Queue.DeclareOk connCheck;
        // connection objects
        Connection conn = null;
        Channel channel = null;
        // config holders
        RabbitMqConfig rabbitMqConfig = null;
        DbConfig dbConfig = null;


        // Read the parameters
        // Read the properties file if available
        Properties prop = getProperties(args[0]);
        if (prop != null) {
            rabbitMqConfig = new RabbitMqConfig(prop);
            dbConfig = new DbConfig(prop);
        // Else try to read the parameters from the command line
        } else {
            try {
                rabbitMqConfig = new RabbitMqConfig(
                        args[0],
                        5672,
                        args[1],
                        args[2],
                        "/",
                        args[3],
                        "messages",
                        "result",
                        "send_back",
                        args[4],
                        "rpc_exchange",
                        args[5]
                );
                dbConfig = new DbConfig(
                        args[6],
                        args[7],
                        args[8],
                        args[9],
                        args[10],
                        args[11]
                );
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Failed to read config params. Check the attached README file for correct parameter format.");
                System.exit(1);
            }
        }

        // Initialize Mysql and RabbitMQ connections using the parameters

        DbConnector dbConnector = null;
        try {
            dbConnector = DbConnectionFactory.createConnection(dbConfig);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(rabbitMqConfig.getUsername());
        factory.setPassword(rabbitMqConfig.getPassword());
        factory.setVirtualHost(rabbitMqConfig.getVirtualHost());
        factory.setHost(rabbitMqConfig.getHost());
        factory.setPort(rabbitMqConfig.getPort());

        // Try to open connection channel with the provided params. Terminate the app on failure
        try {
            conn = factory.newConnection();
            if (conn == null) throw new IOException("Connection cannot be established");
            channel = conn.createChannel();
            if (channel == null) throw new IOException("Channel cannot be reached");
                    } catch (IOException | TimeoutException | AlreadyClosedException e) {
            System.out.println("Rabbit MQ connection failed. Exiting...");
            System.exit(-1);
        }
        try {
            channel.exchangeDeclarePassive(rabbitMqConfig.getAdHocExchange());
        } catch (IOException e) {
            System.out.println("Rabbit MQ connection failed. Cannot connect to exchange \"" + rabbitMqConfig.getAdHocExchange() + "\", exiting. Make sure that the exchange exists on the broker. This can usually be fixed by either creating the exchange manually or sending one request to the Ad-Hoc queue through the backend service.");
            System.exit(-1);
        }
        while (true) {
            // try to connect to queue. Any exception on this function means that queue is not available. Set conncheck to null
            try {
                connCheck = channel.queueDeclarePassive(rabbitMqConfig.getAdHocQueue());
            } catch (IOException e) {
                connCheck = null;
                if (!channel.isOpen()) {
                    // recreate channel as rabbit closes it on IOError
                    try {
                        channel = conn.createChannel();
                    } catch (AlreadyClosedException | IOException ace) {
                        System.out.println("Connection closed. Exiting.");
                    }
                }
            }
            // waiting for AdHoc Queue to come online
            if (!connected) {
                System.out.println("connecting...");
            }
            // connection dropped logic
            if (connCheck == null) {
                if (connected) {
                    connected = false;
                    System.out.println("Connection dropped");
                }
            // connection (re)esablished logic
            } else {
                if (!connected) {
                    connected = AdHocConsumer.connectToQueue(rabbitMqConfig, channel, dbConnector, rabbitMqConfig.getAdHocQueue(), rabbitMqConfig.getAdHocExchange(), false);
                    RPCServer.start(rabbitMqConfig, channel, dbConnector);
                }
            }
            // do nothing loop
            if (connected) {
                System.out.println("tick");
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Properties getProperties(String propertiesFile) {
        try (InputStream input = UserClientApp.class.getClassLoader().getResourceAsStream("./" + propertiesFile)) {

            Properties prop = new Properties();

            if (input == null) {
                System.out.println("Unable to find configuration file: " + propertiesFile + ". Trying to read args...");
                return null;
            }

            prop.load(input);
            return prop;

        } catch (IOException e) {
            System.out.println("Unable to read configuration file: " + propertiesFile + ". Exiting...");
            System.exit(1);
        }
        return null;
    }
}
