package com.rivile.user_client.factories;

import com.rivile.user_client.config.DbConfig;
import com.rivile.user_client.connectors.DbConnector;
import com.rivile.user_client.connectors.MysqlConnector;
import com.rivile.user_client.connectors.SqlserverConnector;

public class DbConnectionFactory {
    public static DbConnector createConnection(DbConfig dbConfig) throws Exception {
        switch (dbConfig.getType()) {
            case "mysql":
                return new MysqlConnector(dbConfig);
            case "sqlserver":
                return new SqlserverConnector(dbConfig);
            default:
                throw new Exception("No or unsupported database type provided");
        }
    }
}
