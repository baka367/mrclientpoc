package com.rivile.user_client;

import com.rivile.user_client.dto.QueryMessageDto;
import com.rivile.user_client.dto.QueryResponseDto;
import com.rivile.user_client.dto.QueryResult;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MessageParser {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MessageParser.class);

    public static QueryMessageDto parseQueryMessage(String message) {
        ObjectMapper mapper = new ObjectMapper();

        /*
         * This allows the ObjectMapper to accept single values for a collection.
         * For example: "location" property in the returned JSON is a collection that
         * can accept multiple objects but, in deserialization process, this property just
         * have one object and causes an Exception.
         */
        mapper.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        /*
         * If some JSON property is not present, avoid exceptions setting
         * FAIL_ON_UNKNOWN_PROPERTIES to false
         */
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        /*
         * Deserialize enums
         */
        mapper.enable(Feature.READ_ENUMS_USING_TO_STRING);

        try {
            return mapper.readValue(message, new TypeReference<QueryMessageDto>() {});
        }  catch (JsonParseException e) {
            logger.error("Invalid message received: " + message);
            return null;
         }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parseQueryResult(QueryResult queryResult) {
        return parseObject(queryResult);
    }

    public static String parseQueryResponse(QueryResponseDto queryResponse) {
        return parseObject(queryResponse);
    }

    private static String parseObject(Object object) {
        ObjectMapper mapper = new ObjectMapper();

        /*
         * This allows the ObjectMapper to accept single values for a collection.
         * For example: "location" property in the returned JSON is a collection that
         * can accept multiple objects but, in deserialization process, this property just
         * have one object and causes an Exception.
         */
        mapper.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        /*
         * If some JSON property is not present, avoid exceptions setting
         * FAIL_ON_UNKNOWN_PROPERTIES to false
         */
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
