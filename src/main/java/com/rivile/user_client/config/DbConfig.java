package com.rivile.user_client.config;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Properties;

@Getter @AllArgsConstructor
public class DbConfig {
    String type;
    String host;
    String port;
    String username;
    String password;
    String db;

    public DbConfig(Properties config) {
        type = config.getProperty("db.type");
        host = config.getProperty("db.host");
        port = config.getProperty("db.port");
        username = config.getProperty("db.username");
        password = config.getProperty("db.password");
        db = config.getProperty("db.db");
    }
}
