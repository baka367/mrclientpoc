package com.rivile.user_client.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Properties;

@Getter @AllArgsConstructor
public class RabbitMqConfig {
    String host;
    int port;
    String username;
    String password;
    String virtualHost;
    String adHocQueue;
    String adHocExchange;
    String adHocSendBackQueue;
    String adHocSendBackExchange;
    String rpcQueue;
    String rpcExchange;
    String rpcRoutingKey;

    public RabbitMqConfig(Properties config) {
        host = config.getProperty("broker.host");
        port = Integer.parseInt(config.getProperty("broker.port"));
        username = config.getProperty("broker.username");
        password = config.getProperty("broker.password");
        virtualHost = config.getProperty("broker.virtualHost");
        adHocQueue = config.getProperty("broker.adHoc.queue");
        adHocExchange = config.getProperty("broker.adHoc.exchange");
        adHocSendBackQueue = config.getProperty("broker.adHoc.sendBackQueue");
        adHocSendBackExchange = config.getProperty("broker.adHoc.sendBackExchange");
        rpcQueue = config.getProperty("broker.rpc.queue");
        rpcExchange = config.getProperty("broker.rpc.exchange");
        rpcRoutingKey = config.getProperty("broker.rpc.routingKey");
    }
}
