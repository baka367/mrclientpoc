package com.rivile.user_client.config;

public class QueryCommand {
    public static final String CMD_UQ_SPEU = "uq_speu"; //user query, stored procedure execute
    public static final String CMD_UQ_SPEQ = "uq_speq"; //user query, stored procedure execute
    public static final String CMD_SPU_CV = "spu_cv"; //stored procedure update, check version
    public static final String CMD_SPU_UP = "spu_up"; //stored procedure update, update procedure
    public static final String CMD_SPU_DP = "spu_dp"; //stored procedure update, delete procedure
    public static final String CMD_SPU_CP = "spu_cp"; //stored procedure update, create procedure
}
