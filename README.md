# DB Remote client PoC

### Introduction
This is a proof of concept client that utilized rabbitMQ to support remote execution of MySQL queries in standard publisher-subscriber (adHoc) or RPC modes.

### Building
Source code is built by running `mvn clean compile assembly:single` in any terminal in the projects root directory. This will pack all the dependencies, including database connectors, to provide a single executable, found in the `target` directory, that can be run as long as client OS has java 8 (or later) installed.

### Launching
Requires a rabbitMQ server, a redis instance (required for rivile-client-comm) and Mano Rivile backend rivile-client-comm service to be running on the backend side. For testing purposes it can be run locally or on a remote server.

- If the project is started from a compiled JAR archive, a set of command line arguments have to be provided to properly initialize the application:
`java -jar target/rivile-user-client-1.0.0-SNAPSHOT-jar-with-dependencies.jar <rabbitMqIp> <rabbitMqUsername> <rabbitMqPassword> <adHocQeue> <rpcQueue> <rpcRoutingKey> <dbConnectionType> <dbIp> <dbPort> <dbUser> <dbPass>  <dbDatabase>`
- If the project is stared from debugger, the user can also use `.properties files` to pass the queue parameters by passing the file name to the debugger as the command line argument: i.e.`user_1.properties`

##### Currently supporting `mysql` and `sqlserver` db types
### Tips
When connecting using the default docker from the backend project, following parameters (ip addresses might need to be adjusted) should be used:
 `java -jar target/rivile-user-client-1.0.0-SNAPSHOT-jar-with-dependencies.jar 192.168.99.100 user password queue rpc_1 rpc_1 mysql 192.168.99.100 3306 root root test`
